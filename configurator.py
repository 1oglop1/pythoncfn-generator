"""Module to setup the app configuration."""
import logging

import toml

LOGGER = logging.getLogger(__name__)

LOGGER.info("Loading config.")
CONFIG = toml.load("config.toml")
