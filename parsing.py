"""Does everything related to parsing."""
import json
from operator import itemgetter

# notes
# property can have sub properties
# from datetime import datetime as dt
from typing import List, Dict

# mapping ###
# DOCUMENTATION = str
# DUPLICATES_ALLOWED = bool
ITEM_TYPE_MAP = {"List": List, "Map": Dict}  # list[Property], list[PrimitiveType],
PRIMITIVE_ITEM_TYPE_MAP = {"List": List, "Map": Dict}
MAP_PRIMITIVE2PYTHON = {
    "String": "str",
    "Long": "int",
    "Integer": "int",
    "Double": "float",
    "Boolean": "bool",
    # TODO fix what happens with timespamp
    "Timestamp": "dt.isoformat",
}
# REQUIRED = bool  # correctly parsed from json
TYPE_MAP = {"List": List, "Map": Dict}  # list[ItemType/Property], list[PrimitiveType], dict[ItemType/Property:],
# UPDATETYPE = "Mutable, Immutable, or Conditional"


def parse_aws_name(name: str):
    """
    Split AWS::aws-product-name::data-type-name.

    Parameters
    ----------
    name: str
        AWS resource name: AWS::aws-product-name::data-type-name

    Returns
    -------
    dict
        Dictionary containing split version of AWS name
        parsed_name = {
            'aws': "AWS",
            'module': product_name,
            'class': resource,
            'property': aws_property,
        }

    """
    # AWS::aws-product-name::data-type-name
    try:
        aws, product_name, data_type_name = name.split("::")
    except ValueError:
        aws = None
        product_name = "common"
        data_type_name = name

    try:
        resource, aws_property = data_type_name.split(".")
    except ValueError:
        resource = data_type_name
        aws_property = None

    parsed_name = {"aws": aws, "module": product_name, "class": resource, "property": aws_property}
    return parsed_name


def concat_aws_name(aws="AWS", module=None, _class=None, _property=None):
    """Concate aws name from parts."""
    aws_name = {"aws": aws, "module": module, "class": _class, "property": _property}

    base = f"{aws_name['aws']}::{aws_name['module']}::{aws_name['class']}"

    if aws_name["property"]:
        return f"{base}.{aws_name['property']}"
    return base


def get_models(path):
    """
    Load models from json file.

    Parameters
    ----------
    path
        Path to the file.

    Returns
    -------
    dict
        Dictionary with models.

    """
    with open(path, "r") as inf:
        return json.load(inf)


def map_list_map2python(item, item_type, optional=False):
    """
    Map CFN item and type into typing.

    Examples
    --------
    >>> item = "StageKey", item_type = 'List'
    ... map_list_map2python(item, item_type)
    ... "typing.List['StageKey']"

    Parameters
    ----------
    item: str
        Resource or Property name.
    item_type: str
        Type of item (Map, List) from cfn spec.
    optional: bool

    Returns
    -------
    str
        typing.Type['item']

    """
    result = None

    if item_type == "List":
        # return List[item]
        result = f"typing.List['{item}']"
    elif item_type == "Dict":
        # Fixme not good
        # return Dict[str, item_type]
        result = f"typing.Dict[str, '{item}']"

    if optional:
        return f"typing.Optional[{result}]"
    return result


def parse_cfn_object(model_name: str, model_properties: dict) -> dict:
    """
    Parse CFN object (resource/property) into dict.

    Parameters
    ----------
    model_name: str
        AWS type name
    model_properties: dict
        CFN item properties, attributes, etc...

    {
    "aws": "AWS",
    "module": "ApiGateway",
    "class": "ApiKey",
    "property": null,
    "properties": [
            {
            "name": "CustomerId",
            "type": "str",
            "required": false,
            "UpdateType": "Mutable"
            },
        ]
    "attributes": []
    }

    """
    # parse 1 model
    name = model_name

    obj_dict = parse_aws_name(name)
    obj_properties = obj_dict.get("properties", [])

    sub_model_properties = model_properties.get("Properties")
    if sub_model_properties:
        obj_dict["properties"] = parse_properties(sub_model_properties)

    model_attributes = model_properties.get("Attributes")
    if model_attributes:
        obj_dict["attributes"] = parse_attributes(model_attributes)
    # obj_dict['_cfn'] = model_properties
    # put required properties first
    obj_properties.sort(key=itemgetter("required"), reverse=True)
    obj_dict["__doc__"] = f"Documentation: {model_properties.get('Documentation')}\n"

    return obj_dict


def parse_properties(properties: dict) -> list:
    """
    Parse resource properties.

    Parameters
    ----------
    properties

    Returns
    -------
        List of properties.

        prop_dict = {
                "name": prop_name,
                "type": None,
                "required": props.get("Required"),
                "UpdateType": props.get("UpdateType"),
        }

    """
    props = []
    for prop_name, attributes in properties.items():
        # here we translate primitive types into python primitives
        # and create objects for non primitive types
        # PrimitiveType
        # primitiveItemtype + Type
        # primitiveItemType
        # Type ( -> MaintenanceWindowRunCommandParameters
        # ItemType + Type (Map, List)
        prop_dict = {
            "name": prop_name,
            "type": None,
            "required": attributes.get("Required"),
            "UpdateType": attributes.get("UpdateType"),
        }
        # type_props = {
        #     '__module__': obj_dict['module']
        # }
        # primitive
        p_type = attributes.get("PrimitiveType")
        p_item_type = attributes.get("PrimitiveItemType")
        # non primitive
        np_item_type = attributes.get("ItemType")
        np_type = attributes.get("Type")
        # primitive
        if p_type and p_item_type:
            prop_dict["type"] = map_list_map2python(
                MAP_PRIMITIVE2PYTHON[p_type], p_item_type, optional=prop_dict["required"]
            )
        elif p_type:
            prop_dict["type"] = MAP_PRIMITIVE2PYTHON[p_type]
        # no primitive
        elif np_item_type and np_type:
            # t = type(np_item_type, (), type_props)
            # print(t)
            prop_dict["type"] = map_list_map2python(np_item_type, np_type, optional=prop_dict["required"])
        elif np_type:
            # t = type(np_type, (), type_props)
            prop_dict["type"] = np_type

        props.append(prop_dict)

        # TODO is this safe?
        sub_property_props = attributes.get("Properties")
        if sub_property_props:
            parse_cfn_object(prop_name, sub_property_props)
    return props


def parse_attributes(attributes) -> list:
    """
    Parse resource attributes - should do (Fn::GetAtt).

    Parameters
    ----------
    attributes
        Resource attributes.

    Returns
    -------
    list of attributes
        { "name": name, "type": type }

    """
    # "ItemType": "Return list or map type (non-primitive)",
    # "PrimitiveItemType": "Return list or map type (primitive)",
    # "PrimitiveType": "Return value type (primitive)",
    # "Type": "Return value type (non-primitive)",
    result = []
    for name, fields in attributes.items():
        attr_dict = {"name": name, "type": None}
        np_item_type = fields.get("ItemType")
        np_type = fields.get("Type")
        p_item_type = fields.get("PrimitiveItemType")
        p_type = fields.get("PrimitiveType")

        if p_type and p_item_type:
            attr_dict["type"] = map_list_map2python(MAP_PRIMITIVE2PYTHON[p_type], p_item_type)
        elif p_type:
            attr_dict["type"] = MAP_PRIMITIVE2PYTHON[p_type]
        # no primitive
        elif np_item_type and np_type:
            # t = type(np_item_type, (), type_props)
            # print(t)
            attr_dict["type"] = map_list_map2python(np_item_type, np_type)
        elif np_type:
            # t = type(np_type, (), type_props)
            attr_dict["type"] = np_type
        result.append(attr_dict)

    return result
