"""Dynamically generate CFN objects for template creation."""
import logging
import sys
from pathlib import Path

from configurator import CONFIG
from parsing import parse_cfn_object, get_models
from rendering import render_class, render_module

STD_HANDLER = logging.StreamHandler(sys.stdout)
LOGGER = logging.getLogger(__name__)
LOGGER.addHandler(STD_HANDLER)
LOGGER.setLevel(logging.DEBUG)

# cfn_resource_specification_file = Path("spec/ApiGatewayDocumentationPartSpecification.json")
CFN_RESOURCE_SPECIFICATION_FILE = Path(f"{CONFIG['folders']['sample_files']}/{CONFIG['files']['sample_cfn_spec']}")

CFN_MODELS = get_models(CFN_RESOURCE_SPECIFICATION_FILE)


def doit():
    """Parse cfn spec into the python modules."""
    modules = {}
    # modules.get()
    for res_prop in ["PropertyTypes", "ResourceTypes"]:
        for resource_name, resource in CFN_MODELS[res_prop].items():
            cls_obj = parse_cfn_object(resource_name, resource)
            mod_name = cls_obj["module"]
            mod_list = modules.get(mod_name, [])
            if not mod_list:
                modules[mod_name] = mod_list

            cls_name = cls_obj.get("property") or cls_obj.get("class")
            # print(srt)
            mod_list.append(
                render_class(
                    cls_name,
                    class_properties=cls_obj["properties"],
                    class_attributes=cls_obj.get("attributes") or "",
                    class_docstring=cls_obj["__doc__"],
                    decorators=["@attr.s"],
                )
            )

        # print(json.dumps(modules, indent=2))

        for mod_name, mod_classes in modules.items():
            print(f"---{mod_name}----")
            rendered_module = render_module(mod_classes, imports=["import typing", "import attr"])
            print(rendered_module)

            pth = Path(f"{CONFIG['folders']['product_destination']}/{mod_name}.py")
            with open(pth, "w+") as outf:
                outf.write(rendered_module)
            print("------------------")


if __name__ == "__main__":
    # for name, model in cfn_models['ResourceType'].items():
    # for name, model in x.items():
    #     # model = x
    #     # method1(model)
    #     create_prop(model, name)
    # print("Resource Types")
    # for resource_name, resource in cfn_models['ResourceTypes'].items():
    #     print(json.dumps(parse(resource_name, resource), indent=2))
    #     break

    # print("Property Types")
    # for resource_name, resource in cfn_models['PropertyTypes'].items():
    #     # list(parse(resource_name, resource))
    #     print(parse(resource_name, resource))
    #     break

    # j2fun()
    doit()
