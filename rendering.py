"""Everything about rendering CFN into python."""
import logging
import typing

import jinja2 as j2

from configurator import CONFIG

LOGGER = logging.getLogger(__name__)


LOGGER.info("Create j2 env.")
J2_ENV = j2.Environment(
    loader=j2.FileSystemLoader(searchpath=CONFIG["folders"]["templates"]),
    trim_blocks=True,
    # lstrip_blocks=True,
    keep_trailing_newline=True,
)

LOGGER.info("Loading templates")
FUNCTION_TEMPLATE = J2_ENV.get_template("function.py.j2")
CLASS_TEMPLATE = J2_ENV.get_template("class.py.j2")
MODULE_TEMPLATE = J2_ENV.get_template("module.py.j2")


# def j2fun():
#     j2_env = j2.Environment(
#         loader=j2.FileSystemLoader(searchpath="pythoncfn/templates"),
#         trim_blocks=True,
#         # lstrip_blocks=True,
#         keep_trailing_newline=True,
#     )
#     context = {
#         # 'function_name': 'hello_world',
#         "function_args": "self, arg1, arg2: List[int], arg3=3, arg4: List[int]=None",
#         "function_body": ("print('lol')\n" "print('lol2')"),
#     }
#     # j2.environment.na
#     # fun_template._c
#     rendered_fn = fun_template.render(**context, function_name="__init__")
#     rendered_fn2 = fun_template.render(
#         {**context, "function_name": "fn2", "decorators": ["@property", "@decorator2"]}
#     )
#     xx = [rendered_fn, rendered_fn2]
#     cls_1 = cls_template.render(
#         class_body="\n".join(xx), class_name="HelloWorld", class_args="Grandma, Grandpa"
#     )
#     cls_2 = cls_template.render(class_body="\n".join(xx), class_name="GoodByeWorld")
#
#     rendered_mdl = mdl_template.render(
#         imports=["from typing import List"],
#         # module_functions='',
#         module_classes=[cls_1, cls_2],
#     )
#
#     print("------------module-------")
#     print(rendered_mdl)


def render_class(
    class_name: str,
    class_docstring: str,
    class_properties: dict,
    class_attributes: dict,
    decorators: typing.List[str] = None,
):
    """
    Render python class as string.

    Parameters
    ----------
    class_name
    class_docstring
    class_properties
        String built by build_cls_body
    class_attributes
        CFN attributes #TODO not implemented
    decorators
        List of class decorators

    Returns
    -------
    str
        String representation of python class.

    """
    cls_body = build_cls_body(class_properties)
    cls_body = f"{cls_body}\nattributes = attr.ib(init=False, default={class_attributes.__repr__()})"

    return CLASS_TEMPLATE.render(
        class_name=class_name, class_body=cls_body, class_docstring=class_docstring, decorators=decorators
    )


def build_cls_body(properties):
    """
    Build class body - properties.

    Parameters
    ----------
    properties
        Properties parsed from CFN

    Returns
    -------
        String of class attributes separated by new line.

    """
    # attr.ib(type=typing.List[int], kw_only=True)
    props = []
    for prop in properties:
        a_name = prop["name"]
        a_type = prop["type"]
        if not prop["required"]:
            inst_prop = f"{a_name} = attr.ib(type={a_type}, kw_only={not prop['required']}, default=None)"
        else:
            inst_prop = f"{a_name} = attr.ib(type={a_type})"

        props.append(inst_prop)
    return "\n".join(props)


def render_module(
    module_classes: typing.List[str], imports: typing.List[str] = None, module_functions: typing.List[str] = None
):
    """
    Render python module as string.

    Parameters
    ----------
    module_classes
        List of classes which should be in the module.
    imports
        Python imports (line by line)
    module_functions
        Rendered functions

    Returns
    -------
        String representation of python module.

    """
    return MODULE_TEMPLATE.render(
        module_classes=module_classes, imports=imports or [], module_functions=module_functions or []
    )
